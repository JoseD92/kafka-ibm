ibm_brokers:
	terraform output -json | jq -r '.test_topic_sasl.value | join("\n")'

ibm_rest_endpoint:
	terraform output -json | jq -r '.test_topic_http_url.value'

ibm_api_key:
	terraform output -json | jq -r '.es_api_key.value'

run_python_ibm_producer:
	cd kafka-python-console-sample; \
	  python app.py $$(cd ..; make -s ibm_brokers | shuf -n1) \
	  	$$(cd ..; make -s ibm_rest_endpoint) \
		$$(cd ..; make -s ibm_api_key) \
		'/etc/ssl/certs' \
		-producer

run_python_ibm_consumer:
	cd kafka-python-console-sample; \
	  python app.py $$(cd ..; make -s ibm_brokers | shuf -n1) \
	  	$$(cd ..; make -s ibm_rest_endpoint) \
		$$(cd ..; make -s ibm_api_key) \
		'/etc/ssl/certs' \
		-consumer

init:
	cd kafka-python-console-sample; \
	  pip install -r requirements.txt