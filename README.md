## IBM Event Streams

Compared to a regular self hosted kafka service, ibm es offers easy scalability (both in persistent space and resources) and geo replication, it can be easily manage using terraform, see [example](main.tf), it only requires creating a `ibm_resource_instance` and then topics can be created in that instance as needed. From terraform can also be managed the api key for accessing the service using the `ibm_resource_key` resource, but this api keys are valid for all topics in the instance with no fine control.

Each `ibm_resource_instance` counts as a separate kafka service and permissions are separate from each other.

Apps that require using kafka topics created using terraform, only need the api_key and sasl services urls, all of which can be set as an output on terraform. See [app example](kafka-python-console-sample), this is a python program that pushes messages or consumes messages from an ibm es instance.

Ibm es also supports other features like schemas, to prevent incorrect data from being pushed into a topic and connectors but it does not appears like that can be managed from terraform which makes using more cumbersome.

Ibm used to have a self managed version of IBM Event Streams but it has since been deprecated, they [recommend](https://ibm.github.io/event-streams/installing/installing/) using Confluent Platform, they do provide docker containers and charts for installing kafka, and the changes required in the test program would be minimal, but it is a harder install and we need to manage the persistent volumes, replication and many other details manually. See [Quick Start for Apache Kafka using Confluent Platform (Docker)](https://docs.confluent.io/platform/current/quickstart/ce-docker-quickstart.html) for more information. Comparing other docker offerings of kafka, Confluent Platform does looks more robust and friendlier to use, it offers a control panel to manage topics and connectors from a web interface.

## ibm event streams vs redis

IBM cloud provides a managed redis database and the terraform provider does support their management, but there is no free version so no way to try it.

Event streams appears to be better integrated with other ibm cloud products like foundry apps, but the decision comes down to the fundamentals between kafka and redis.

## To investigate

Some aspects of ibm cloud implementation of kafka are still not clear, like:

* How to export saved data, there is no easy way to download the persisted data for migration or diving, maybe making an app that queries everything or through some connector.
* Because ibm event streamer runs on a Multi-tenant architecture for both the standard and lite plans, the connection to the apps can not be network isolated. it is not know if the enterprise plan that runs on a single tenant instance could be network isolated for further security, no docs I have seen so far indicates so, so we need to keep using the app_key's.
* Changing options for the topics on terraform may destroy the data given how terraform tends to operate, we need to watch out for what options may cause a destruction of the topics. Use `plan` before `apply`.
* Using connectors for ibm event streams looks to be significantly more difficult compared with self hosting, with self hosting only adding jar files on directories and config files are required, for ibm es it looks like IBM App Connect needs to be used, and that is its own set of problems, see https://www.ibm.com/docs/en/app-connect/cloud?topic=apps-event-streams. Also I don't see a way to use IBM App Connect from terraform.

## Running test

After applying the terraform changes, use the commands `make init` to download python dependencies, and then use `make run_python_ibm_producer` and `make run_python_ibm_consumer` in separate terminals to run event producer and consumer.