terraform {
  required_providers {
    ibm = {
      source = "IBM-Cloud/ibm"
      version = "~> 1.12.0"
    }
  }
}

provider "ibm" {
  region = "us-south"
}

# can't use more resource groups in standard account
#resource "ibm_resource_group" "kafkaTestGroup" {
#  name     = "kafkaTestGroup"
#}

resource "ibm_resource_instance" "es_instance" {
  name              = "kafka-test-es"
  service           = "messagehub"
  plan              = "lite"
  location          = "us-south"
  #resource_group_id = ibm_resource_group.kafkaTestGroup.id
}

resource "ibm_event_streams_topic" "es_topic" {
  resource_instance_id = ibm_resource_instance.es_instance.id
  name                 = "test-topic"
  partitions           = 1
#   config = { # config not supported in free plan
#   }
}

resource "ibm_resource_key" "es_api_key" {
  name                 = "es_api_key"
  role                 = "Manager"
  resource_instance_id = ibm_resource_instance.es_instance.id
}

output "test_topic_sasl" {
  value = ibm_event_streams_topic.es_topic.kafka_brokers_sasl
}

output "test_topic_http_url" {
  value = ibm_event_streams_topic.es_topic.kafka_http_url
}

output "es_api_key" {
  value = ibm_resource_key.es_api_key.credentials["api_key"]
  sensitive = true
}